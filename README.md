```
Usage: lcgview.py [options] <view_root>                                                                                                           

This package creates a "view" of LCG release in folder <view_root>.

optional arguments:
    -h, --help                       show this help message and exit
    -p LCGPATH, --lcgpath LCGPATH    top directory of LCG releases (default: /afs/cern.ch/sw/lcg/releases)
    -r LCGREL, --release LCGREL      LCG release number (default: 80)
    -l LCGPLAT, --platform LCGPLAT   Platform to use (default: x86_64-slc6-gcc49-opt)
    -d, --delete                     delete old view before creating a new one
    -v, --version                    show program's version number and exit
```