#!/usr/bin/env python
import argparse
import os
import sys
import shutil


def get_externals(lcg_file):
    externals = {}
    txt_file = open(lcg_file)
    for line in txt_file.readlines():
        try:
            name, pkghash, version, home, deps = [x.strip() for x in line.split(';')]
        except ValueError:
            continue
        else:
            r = {'NAME': name, 'HASH': pkghash, 'HOME': home,
                 'VERSION': version}
            externals.update({name: r})

    return externals


def splitall(path):
    allparts = []
    while 1:
        parts = os.path.split(path)
        if parts[0] == path:  # sentinel for absolute paths
            allparts.insert(0, parts[0])
            break
        elif parts[1] == path: # sentinel for relative paths
            allparts.insert(0, parts[1])
            break
        else:
            path = parts[0]
            allparts.insert(0, parts[1])
    return allparts


def main():
    helpstring = """{0} [options] <view_destination>

This package creates a "view" of LCG release in folder <view_destination>.
"""

    # lcg_root = '/afs/cern.ch/sw/lcg/releases'
    # lcg_release = 79
    # lcg_platform = 'x86_64-slc6-gcc49-opt'
    # view_root = '/tmp/view_{0}{1}'.format(lcg_release, lcg_platform)

    parser = argparse.ArgumentParser(usage=helpstring.format(sys.argv[0]))
    parser.add_argument('view_destination', metavar='view_destination', nargs='+', help=argparse.SUPPRESS)
    parser.add_argument('-l', '--lcgpath', help="top directory of LCG releases (default: /afs/cern.ch/sw/lcg/releases)",
                        action="store",
                        default='/afs/cern.ch/sw/lcg/releases', dest='lcgpath')
    parser.add_argument('-r', '--release', help="LCG release number (default: 80)", action="store", default=80, dest="lcgrel")
    parser.add_argument('-p', '--platform', help="Platform to use (default: x86_64-slc6-gcc49-opt)", action="store",
                        default='x86_64-slc6-gcc49-opt', dest='lcgplat')
    parser.add_argument('-d', '--delete', help="delete old view before creating a new one", action="store_true",
                        default=False, dest='delview')
    parser.add_argument('-B', '--enable-blacklists', help='Enable built-in blacklists of files and packages. For experts only.', 
                        action="store_true", default=False, dest='bl_enabled') 
    parser.add_argument('-v', '--version', action='version', version='%(prog)s 0.3')
    args = parser.parse_args()

    lcg_root = args.lcgpath
    lcg_release = args.lcgrel
    lcg_platform = args.lcgplat
    view_root = args.view_destination[0]

    if args.bl_enabled:
        blacklist = ['version.txt', '.filelist', 'easy-install.pth', 'site.py', 'site.pyc', 'README', 'easy_install',
                     'LICENSE', 'decimal.h', 'atan2.h', 'project.cmt', 'INSTALL']
        pkg_blacklist = ['neurobayes_expert', 'pytools', 'pyanalysis', 'vdt', 'cmt', 'Qt5']
    else:
        blacklist = ['version.txt']
        pkg_blacklist = []

    if args.delview and os.path.exists(view_root):
        shutil.rmtree(view_root, True)

    if not os.path.exists(view_root):
        os.makedirs(view_root)

    release_root = os.path.join(lcg_root, 'LCG_%s' % lcg_release)
    lcg_file = os.path.join(release_root, 'LCG_externals_%s.txt' % lcg_platform)

    externals = get_externals(lcg_file)
    for pkg in externals:
        if pkg in pkg_blacklist:
            continue

        pkg_root = os.path.realpath(os.path.join(release_root, externals[pkg]['HOME']))
        print 'Package {0}: root = {1}'.format(pkg, pkg_root)

        for (dir_path, dirnames, filenames) in os.walk(pkg_root, followlinks=True):
            if 'doc' in splitall(dir_path):
                continue

            if 'logs' in splitall(dir_path):
                continue

            dirpath = dir_path.replace(pkg_root, '.')
            view_dir = os.path.realpath(os.path.join(view_root, dirpath))
            if not os.path.exists(view_dir):
                # print 'Create directory', os.path.realpath(os.path.join(view_root, dirpath))
                try:
                    os.makedirs(view_dir)
                except OSError as e:
                    if e.errno == 20:
                        print "*** WARNING: Target already exisits and is file: {0} ***".format(view_dir)
                        print "*** NOTICE: Added from: {0} ***".format(os.path.realpath(view_dir))
                        print "*** NOTICE: Conflicts with: {0} ***".format(os.path.realpath(dir_path))
                    else:
                        raise e

            # continue
            for f in filenames:
                if f in blacklist or f.startswith('.') or f.endswith('-env.sh') or f.endswith('~'):
                    continue

                source = os.path.join(dir_path, f)
                target = os.path.join(view_dir, f)

                source_rel = os.path.realpath(source).replace(lcg_root + os.path.sep, '')
                target_rel = os.path.realpath(target).replace(lcg_root + os.path.sep, '')
                if not os.path.exists(target):
                    # print "Create symlink: {0} -> {1}".format(source, target)
                    try:
                        os.symlink(source, target)
                    except OSError as e:
                        if e.errno == 20:
                            print "*** WARNING: Target already exisits and is file: {0} ***".format(f)
                            print "*** NOTICE: Added from: {0} ***".format(target_rel)
                            print "*** NOTICE: Conflicts with: {0} ***".format(source_rel)
                        else:
                            raise e
                else:
                    print "*** WARNING: File already exisits: {0} ***".format(target)
                    print "*** NOTICE: Added from: {0} ***".format(target_rel)
                    print "*** NOTICE: Conflicts with: {0} ***".format(source_rel)
                    #return 1
    return 0


if __name__ == '__main__':
    exit(main())
